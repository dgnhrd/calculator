<?php

include 'Classes/Controller/CalculatorController.php';
include 'Classes/Service/LogService.php';
include 'Classes/Service/CsvService.php';

$shortOpts = "a:f:";
$longOpts  = ["action:", "file:"];

$calculatorController = new \Neuffer\Calculator\Controller\CalculatorController(getopt($shortOpts, $longOpts));
