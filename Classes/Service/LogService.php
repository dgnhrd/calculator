<?php

namespace Neuffer\Calculator\Service;

class LogService
{
    /**
     * @var string
     */
    const LOG_FILE = 'log.txt';

    public static function log(string $data)
    {
        // If log file exists delete it
        if (file_exists(self::LOG_FILE)) {
            unlink(self::LOG_FILE);
        }
        // Open new log file and write data to it
        $handle = fopen(self::LOG_FILE, 'a+');
        fwrite($handle, $data);
        fclose($handle);
    }
}
