<?php

namespace Neuffer\Calculator\Service;

class CsvService
{
    /**
     * @var string
     */
    const CSV_FILE = 'result.csv';

    public static function writeData(string $data)
    {
        // If csv file exists delete it
        if (file_exists(self::CSV_FILE)) {
            unlink(self::CSV_FILE);
        }
        // Open new csv file and write data to it
        $handle = fopen(self::CSV_FILE, 'a+');
        fwrite($handle, $data);
        fclose($handle);
    }
}
