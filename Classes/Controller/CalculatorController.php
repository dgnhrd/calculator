<?php

namespace Neuffer\Calculator\Controller;

use Neuffer\Calculator\Service\CsvService;
use Neuffer\Calculator\Service\LogService;

class CalculatorController
{
    /**
     * @var array
     */
    private $params;

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $file;

    /**
     * @var array
     */
    private $data;

    /**
     * @var int
     */
    private $counter = 0;

    private $handle;

    public function __construct(array $params)
    {
        $this->params = $params;
        // Get the file path from console command argument
        $this->file =
            !isset($this->params['f']) ? (!isset($this->params['file']) ? null : $this->params['file']) : $this->params['f'];
        // Proof if the action name is set as argument in the console command
        $this->action =
            !isset($this->params['a']) ? (!isset($this->params['action']) ? null : $this->params['action']) : $this->params['a'];
        // Further processing via init function for not blowing up the constructor
        $this->init();
    }

    public function __destruct()
    {
        fclose($this->handle);
    }

    private function init()
    {
        // Is file set and exists it and is readable
        if ($this->file && $this->processFile()) {
            // Is action parameter one of our calculation parameters
            if (in_array($this->action, ['plus', 'minus', 'multiply', 'division'])) {
                // Calculate function
                $this->calculate();
            } else {
                throw new \Exception("Your action is not existing");
            }
        } else {
            throw new \Exception("Please define a file");
        }
    }

    private function processFile(): bool
    {
        // Open file handle if file exists and is readable
        if (file_exists($this->file) && is_readable($this->file)) {
            $this->handle = fopen($this->file, 'r');
        } else {
            throw new \Exception("Your file does not exist or is not readable");
        }

        return true;
    }

    public function calculate()
    {
        // Instanciate data array keys
        $this->data['log'] = [];
        $this->data['csv'] = [];
        // Iterate over the base csv file
        while (($data = fgetcsv($this->handle, 1000, ";")) !== false) {
            // For is not numeric and division through zero set default to < 0 for not ending in result.csv
            $result = -1;
            // Make sure values are numeric - issues with ZWNSP in testings
            if (is_numeric($data[0]) && is_numeric($data[1])) {
                // Choose action and calculate
                switch ($this->action) {
                    case 'plus':
                        $result = $data[0] + $data[1];
                        break;
                    case 'minus':
                        $result = $data[0] - $data[1];
                        break;
                    case 'multiply':
                        $result = $data[0] * $data[1];
                        break;
                    case 'division':
                        // Not allowed division through zero exception - log it if
                        if ($data[1] != 0) {
                            $result = $data[0] / $data[1];
                        } else {
                            $this->data['log'][] = 'numbers ' . $data[0] . ' and ' . $data[1] . ' are wrong, is not allowed';
                        }
                        break;
                }
            } else {
                // one calculation value is not numeric log
                $this->data['log'][] = 'numbers ' . $data[0] . ' and ' . $data[1] . ' are not numeric';
            }
            // if result is greater than zero write it to array for result.csv, if not in array for log.txt
            if ($result >= 0) {
                $this->data['csv'][] = $data[0] . ';' . $data[1] . ';' . $result;
            } else {
                $this->data['log'][] = 'numbers ' . $data[0] . ' and ' . $data[1] . ' are wrong';
            }
            // Increment counter for user feedback of evaluated rows
            $this->counter++;
        }
        // Write log and csv via static function
        LogService::log(implode("\r\n", $this->data['log']));
        CsvService::writeData(implode("\r\n", $this->data['csv']));
        // Return value for user feedback
        echo 'There were ' . $this->counter . ' rows processed!';
    }
}
